import React from 'react'
import './runs.css'
import RunsService from './RunsService';
import swal from 'sweetalert'

import M from "materialize-css/dist/js/materialize.min.js";
import $ from 'jquery';

import AuthService from '../AuthService';
const Auth = new AuthService();

class Runs extends React.Component {

    constructor() {
        super()

        this.state = {
            usuarioLogado: null,
            id: null,
            distance: '',
            duration: '',
            friendly_name: '',
            colRuns: []
        }

        this.handleFormSubmit = this.handleFormSubmit.bind(this)
        this.handleDelRun = this.handleDelRun.bind(this)
        this.handleEditRun = this.handleEditRun.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.getAll = this.getAll.bind(this)

        this.runsService = new RunsService()
    }

    componentWillMount() {
        const profile = Auth.getProfile()
        this.setState({ usuarioLogado: profile })

        this.getAll();
    }

    getAll() {
        this.runsService.getAll().then((res) => {
            this.setState({ colRuns: res })
        }).catch(err => {
            swal({ title: "Erro", text: err, icon: "danger" });
        })
    }

    handleDelRun(obj) {
        this.runsService.excluir(obj.id).then(res => {
            swal({ title: "Aviso", text: 'Registro removido com sucesso', icon: "info" });
            this.getAll();
        });
    }

    handleEditRun(obj) {

        this.setState({
            id: obj.id,
            distance: obj.distance,
            duration: obj.duration,
            friendly_name: obj.friendly_name
        })

        $(document).ready(function () {
            M.updateTextFields();
        });
    }

    handleFormSubmit(e) {

        e.preventDefault();

        if (this.state && this.state.distance && this.state.duration && this.state.friendly_name) {

            this.runsService.salvar(this.state)
                .then(res => {

                    swal({ title: "Aviso", text: "O registro foi salvo com sucesso", icon: "success" });

                    this.setState({
                        id: null,
                        distance: '',
                        duration: '',
                        friendly_name: ''
                    })

                    this.getAll();

                })
                .catch(err => {
                    swal({ title: "Erro", text: err, icon: "danger" });
                })
        } else {
            swal({ title: "Aviso", text: "Preencha corretamente os campos solicitados", icon: "warning" });
        }
    }

    handleCancelar() {
        this.setState({
            id: null,
            distance: '',
            duration: '',
            friendly_name: ''
        })
    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        return (
            <div className='cardRun'>
                <h5>Gerenciamento de Corridas</h5>

                <form onSubmit={this.handleFormSubmit}>

                    <fieldset>
                        <legend>Informações do registro</legend>

                        <div className="input-field col s3">
                            <input id="friendly_name" name="friendly_name" type="text" className="validate" onChange={this.handleChange} value={this.state.friendly_name} />
                            <label htmlFor="friendly_name">Friendly</label>
                        </div>

                        <div className="input-field col s3">
                            <input id="distance" name="distance" type="text" className="validate" onChange={this.handleChange} value={this.state.distance} />
                            <label htmlFor="distance">Distance ({this.state.usuarioLogado.unit})</label>
                        </div>

                        <div className="input-field col s3">
                            <input id="duration" name="duration" type="text" className="validate" onChange={this.handleChange} value={this.state.duration} />
                            <label htmlFor="duration">Duration (seg)</label>
                        </div>

                        <div className="input-field col s1">
                            <button className="waves-effect waves-light btn" type="submit">
                                <i className="material-icons">save</i>
                            </button>

                            <button className="waves-effect waves-light btn gray" type='button' onClick={() => { this.handleCancelar() }}>
                                <i className="material-icons">cancel</i>
                            </button>
                        </div>
                    </fieldset>
                </form>

                {this.state.colRuns.length > 0 &&
                    <table>
                        <thead>
                            <tr>
                                {this.state.usuarioLogado.role === 'admin' &&
                                    <th>Usuário</th>
                                }
                                <th>Titulo</th>
                                <th>Distance</th>
                                <th>Duration (min)</th>
                                <th>Created</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                this.state.colRuns.map((obj, i) => {
                                    return (
                                        <tr key={i}>
                                            {this.state.usuarioLogado.role === 'admin' &&
                                                <td>{obj.name}</td>
                                            }
                                            <td>{obj.friendly_name}</td>
                                            <td>{obj.distance_format}</td>
                                            <td>{obj.duration_format}</td>
                                            <td>{obj.created_format}</td>
                                            <td>
                                                {(this.state.usuarioLogado.role === 'admin' || this.state.usuarioLogado.id === obj.user_id) &&
                                                    <div>
                                                        <a onClick={() => { this.handleEditRun(obj) }} className="btn-floating btn-small waves-effect waves-light orange"><i className="material-icons">edit</i></a>
                                                        <a onClick={() => { this.handleDelRun(obj) }} className="btn-floating btn-small waves-effect waves-light red"><i className="material-icons">delete</i></a>
                                                    </div>
                                                }
                                            </td>
                                        </tr>
                                    )
                                })
                            }
                        </tbody>
                    </table>
                }
            </div>
        )
    }
}

export default Runs